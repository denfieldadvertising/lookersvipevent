# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.17-MariaDB)
# Database: lookersvip
# Generation Time: 2016-10-27 13:43:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table bookings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bookings`;

CREATE TABLE `bookings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slot_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `dealership_id` int(11) unsigned NOT NULL,
  `phone` varchar(64) NOT NULL DEFAULT '',
  `cartype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dealership_id` (`dealership_id`),
  KEY `slot_id` (`slot_id`),
  CONSTRAINT `bookings_ibfk_2` FOREIGN KEY (`dealership_id`) REFERENCES `dealership` (`dealership_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table day
# ------------------------------------------------------------

DROP TABLE IF EXISTS `day`;

CREATE TABLE `day` (
  `day_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`day_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `day` WRITE;
/*!40000 ALTER TABLE `day` DISABLE KEYS */;

INSERT INTO `day` (`day_id`, `name`)
VALUES
	(1,'Thursday <br>10<sup>th</sup> November'),
	(2,'Friday <br>11<sup>th</sup> November'),
	(3,'Saturday <br>12<sup>th</sup> November'),
	(4,'Sunday <br>13<sup>th</sup> November');

/*!40000 ALTER TABLE `day` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dealership
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dealership`;

CREATE TABLE `dealership` (
  `dealership_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  PRIMARY KEY (`dealership_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `dealership` WRITE;
/*!40000 ALTER TABLE `dealership` DISABLE KEYS */;

INSERT INTO `dealership` (`dealership_id`, `name`, `address`)
VALUES
	(1,'Sunderland','<strong>Lookers Ford Sunderland</strong><br>8 Newcastle Road, Sunderland, <br>SR5 1JQ<br>Tel: 0191 594 5500<br><a href=\"http://www.lookers.co.uk/ford/contact-us/sunderland-ford/\">http://www.lookers.co.uk/ford/contact-us/sunderland-ford/</a>'),
	(2,'Guiseley','<strong>Lookers Ford Guiseley</strong><br>Otley Road, Guiseley,<br>Leeds LS20 8BT<br>Tel: 01943812100<br><a href=\"http://www.lookers.co.uk/ford/contact-us/guiseley-ford/\">http://www.lookers.co.uk/ford/contact-us/guiseley-ford</a>'),
	(3,'Harrogate','<strong>Lookers Ford Harrogate</strong><br>Knaresborough Road, Harrogate,<br>Yorkshire HG2 7LU<br>Tel: 0344 211 0705<br><a href=\"http://www.lookers.co.uk/ford/contact-us/harrogate-ford/\">http://www.lookers.co.uk/ford/contact-us/harrogate-ford/</a>'),
	(4,'Leeds','<strong>Lookers Ford Leeds</strong><br>227 York Road, Leeds,<br>Yorkshire LS9 7RY<br>Tel: 0344 234 9436<br><a href=\"http://www.lookers.co.uk/ford/contact-us/leeds-ford-york/\">http://www.lookers.co.uk/ford/contact-us/leeds-ford-york/</a>'),
	(5,'Sheffield','<strong>Lookers Ford Sheffield</strong><br>10 Savile Street East, Sheffield,<br>South Yorkshire S4 7UQ<br>Tel: 01473 520682<br><a href=\"http://www.lookers.co.uk/ford/contact-us/sheffield-ford-savile-street-east/\">www.lookers.co.uk/ford/contact-us/sheffield-ford-savile-street-east</a>'),
	(6,'Chelmsford','<strong>Lookers Ford Chelmsford</strong><br>2 Argyll Road, Chelmsford,<br>Essex CM26PY<br>Tel: 0344 211 2831 <br><a href=\"http://www.lookers.co.uk/ford/contact-us/chelmsford-ford-argyll-road/\">www.lookers.co.uk/ford/contact-us/chelmsford-ford-argyll-road/</a>\r\n\r\n'),
	(7,'Colchester','<strong>Lookers Ford Colchester</strong><br>61 Magdalen Street, Colchester Magdalen St <br>Essex CO12JU<br>Tel: 0344 234 5772 <br><a href=\"http://www.lookers.co.uk/ford/contact-us/colchester-ford-new-used-cars/\">www.lookers.co.uk/ford/contact-us/colchester-ford-new-used-cars/</a>\r\n\r\n'),
	(8,'Braintree','<strong>Lookers Ford Braintree</strong><br>Rayne Road, Braintree<br>Essex CM7 2QS <br>Tel: 0344 211 6362  <br><a href=\"http://www.lookers.co.uk/ford/contact-us/braintree-ford/\">www.lookers.co.uk/ford/contact-us/braintree-ford/</a>'),
	(9,'South Woodham Ferrers','<strong>Lookers Ford South Woodham Ferrers</strong><br>Haltwhistle Road, Western Industrial Estate, South Woodham Ferrers<br>Essex CM35 ZA<br>Tel: 0344 222 0902  <br><a href=\"http://www.lookers.co.uk/ford/contact-us/south-woodham-ferrers-ford/\">www.lookers.co.uk/ford/contact-us/south-woodham-ferrers-ford/</a>'),
	(10,'Sudbury','<strong>Lookers Ford Sudbury</strong><br>Northern Road,  Chilton Industrial Estate, Sudbury<br>Suffolk CO10 2XQ<br>Tel: 0344 222 8380  <br><a href=\"http://www.lookers.co.uk/ford/contact-us/sudbury-ford/\">www.lookers.co.uk/ford/contact-us/sudbury-ford/</a>');

/*!40000 ALTER TABLE `dealership` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dealership_mail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dealership_mail`;

CREATE TABLE `dealership_mail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dealership_id` int(11) unsigned NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `dealership_id` (`dealership_id`),
  CONSTRAINT `dealership_mail_ibfk_1` FOREIGN KEY (`dealership_id`) REFERENCES `dealership` (`dealership_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `dealership_mail` WRITE;
/*!40000 ALTER TABLE `dealership_mail` DISABLE KEYS */;

INSERT INTO `dealership_mail` (`id`, `dealership_id`, `email`)
VALUES
	(1,1,'lee.mason@benfieldmotorgroup.com'),
	(2,1,'chris.nixon@benfieldmotorgroup.com'),
	(3,2,'dave.cavanagh@benfieldmotorgroup.com'),
	(4,2,'richard.preston@benfieldmotorgroup.com'),
	(5,3,'david.lister@benfieldmotorgroup.com'),
	(10,3,'david.dewhurst@benfieldmotorgroup.com'),
	(11,4,'nigel.clayton@benfieldmotorgroup.com'),
	(12,4,'richard.bradley@benfieldmotorgroup.com'),
	(13,5,'ianwilson@lookers.co.uk'),
	(14,5,'johnsawyer@lookers.co.uk');

/*!40000 ALTER TABLE `dealership_mail` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table slot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `slot`;

CREATE TABLE `slot` (
  `slot_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dealership_id` int(11) unsigned NOT NULL,
  `day_id` int(11) unsigned NOT NULL,
  `time_id` int(11) unsigned NOT NULL,
  `num_appointments` int(11) NOT NULL,
  PRIMARY KEY (`slot_id`),
  KEY `dealership_id` (`dealership_id`),
  KEY `day_id` (`day_id`),
  KEY `time_id` (`time_id`),
  CONSTRAINT `slot_ibfk_1` FOREIGN KEY (`dealership_id`) REFERENCES `dealership` (`dealership_id`) ON DELETE CASCADE,
  CONSTRAINT `slot_ibfk_2` FOREIGN KEY (`day_id`) REFERENCES `day` (`day_id`) ON DELETE CASCADE,
  CONSTRAINT `slot_ibfk_3` FOREIGN KEY (`time_id`) REFERENCES `time` (`time_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `slot` WRITE;
/*!40000 ALTER TABLE `slot` DISABLE KEYS */;

INSERT INTO `slot` (`slot_id`, `dealership_id`, `day_id`, `time_id`, `num_appointments`)
VALUES
	(1,1,1,1,4),
	(2,1,1,2,4),
	(3,1,1,3,4),
	(4,1,1,4,4),
	(5,1,1,5,4),
	(6,1,1,6,4),
	(7,1,1,7,4),
	(8,1,1,8,4),
	(9,1,1,9,4),
	(10,1,1,10,4),
	(11,1,2,1,4),
	(12,1,2,2,4),
	(13,1,2,3,4),
	(14,1,2,4,4),
	(15,1,2,5,4),
	(16,1,2,6,4),
	(17,1,2,7,4),
	(18,1,2,8,4),
	(19,1,2,9,4),
	(20,1,3,1,4),
	(21,1,3,2,4),
	(22,1,3,3,4),
	(23,1,3,4,4),
	(24,1,3,5,4),
	(25,1,3,6,4),
	(26,1,3,7,4),
	(27,1,4,2,4),
	(28,1,4,3,4),
	(29,1,4,4,4),
	(30,1,4,5,4),
	(31,1,4,6,4),
	(32,2,1,1,4),
	(33,2,1,2,4),
	(34,2,1,3,4),
	(35,2,1,4,4),
	(36,2,1,5,4),
	(37,2,1,6,4),
	(38,2,1,7,4),
	(39,2,1,8,4),
	(40,2,1,9,4),
	(41,2,1,10,4),
	(42,2,2,1,4),
	(43,2,2,2,4),
	(44,2,2,3,4),
	(45,2,2,4,4),
	(46,2,2,5,4),
	(47,2,2,6,4),
	(48,2,2,7,4),
	(49,2,2,8,4),
	(50,2,2,9,4),
	(51,2,3,1,4),
	(52,2,3,2,4),
	(53,2,3,3,4),
	(54,2,3,4,4),
	(55,2,3,5,4),
	(56,2,3,6,4),
	(58,2,3,7,4),
	(59,2,4,2,4),
	(60,2,4,3,4),
	(61,2,4,4,4),
	(62,2,4,5,4),
	(63,2,4,6,4),
	(64,3,1,1,4),
	(65,3,1,2,4),
	(66,3,1,3,4),
	(67,3,1,4,4),
	(68,3,1,5,4),
	(69,3,1,6,4),
	(70,3,1,7,4),
	(71,3,1,8,4),
	(72,3,1,9,4),
	(73,3,1,10,4),
	(74,3,2,1,4),
	(75,3,2,2,4),
	(76,3,2,3,4),
	(77,3,2,4,4),
	(78,3,2,5,4),
	(79,3,2,6,4),
	(80,3,2,7,4),
	(81,3,2,8,4),
	(82,3,2,9,4),
	(83,3,3,1,4),
	(84,3,3,2,4),
	(85,3,3,3,4),
	(86,3,3,4,4),
	(87,3,3,5,4),
	(88,3,3,6,4),
	(89,3,3,7,4),
	(90,3,4,2,4),
	(91,3,4,3,4),
	(92,3,4,4,4),
	(93,3,4,5,4),
	(94,3,4,6,4),
	(95,4,1,1,4),
	(96,4,1,2,4),
	(97,4,1,3,4),
	(98,4,1,4,4),
	(99,4,1,5,4),
	(100,4,1,6,4),
	(101,4,1,7,4),
	(102,4,1,8,4),
	(103,4,1,9,4),
	(104,4,1,10,4),
	(105,4,2,1,4),
	(106,4,2,2,4),
	(107,4,2,3,4),
	(108,4,2,4,4),
	(109,4,2,5,4),
	(110,4,2,6,4),
	(111,4,2,7,4),
	(112,4,2,8,4),
	(113,4,2,9,4),
	(114,4,3,1,4),
	(115,4,3,2,4),
	(116,4,3,3,4),
	(117,4,3,4,4),
	(118,4,3,5,4),
	(119,4,3,6,4),
	(120,4,3,7,4),
	(121,4,4,2,4),
	(122,4,4,3,4),
	(123,4,4,4,4),
	(124,4,4,5,4),
	(125,4,4,6,4),
	(126,5,1,1,4),
	(127,5,1,2,4),
	(128,5,1,3,4),
	(129,5,1,4,4),
	(130,5,1,5,4),
	(131,5,1,6,4),
	(132,5,1,7,4),
	(133,5,1,8,4),
	(134,5,1,9,4),
	(135,5,1,10,4),
	(136,5,2,1,4),
	(137,5,2,2,4),
	(138,5,2,3,4),
	(139,5,2,4,4),
	(140,5,2,5,4),
	(141,5,2,6,4),
	(142,5,2,7,4),
	(143,5,2,8,4),
	(144,5,2,9,4),
	(145,5,3,1,4),
	(146,5,3,2,4),
	(147,5,3,3,4),
	(148,5,3,4,4),
	(149,5,3,5,4),
	(150,5,3,6,4),
	(151,5,3,7,4),
	(152,5,4,2,4),
	(153,5,4,3,4),
	(154,5,4,4,4),
	(155,5,4,5,4),
	(156,5,4,6,4),
	(157,6,1,1,4),
	(158,6,1,2,4),
	(159,6,1,3,4),
	(160,6,1,4,4),
	(161,6,1,5,4),
	(162,6,1,6,4),
	(163,6,1,7,4),
	(164,6,1,8,4),
	(165,6,1,9,4),
	(166,6,1,10,4),
	(167,6,2,1,4),
	(168,6,2,2,4),
	(169,6,2,3,4),
	(170,6,2,4,4),
	(171,6,2,5,4),
	(172,6,2,6,4),
	(173,6,2,7,4),
	(174,6,2,8,4),
	(175,6,2,9,4),
	(176,6,3,1,4),
	(177,6,3,2,4),
	(178,6,3,3,4),
	(179,6,3,4,4),
	(180,6,3,5,4),
	(181,6,3,6,4),
	(182,6,3,7,4),
	(183,6,4,2,4),
	(184,6,4,3,4),
	(185,6,4,4,4),
	(186,6,4,5,4),
	(187,6,4,6,4),
	(188,7,1,1,4),
	(189,7,1,2,4),
	(190,7,1,3,4),
	(191,7,1,4,4),
	(192,7,1,5,4),
	(193,7,1,6,4),
	(194,7,1,7,4),
	(195,7,1,8,4),
	(196,7,1,9,4),
	(197,7,1,10,4),
	(198,7,2,1,4),
	(199,7,2,2,4),
	(200,7,2,3,4),
	(201,7,2,4,4),
	(202,7,2,5,4),
	(203,7,2,6,4),
	(204,7,2,7,4),
	(205,7,2,8,4),
	(206,7,2,9,4),
	(207,7,3,1,4),
	(208,7,3,2,4),
	(209,7,3,3,4),
	(210,7,3,4,4),
	(211,7,3,5,4),
	(212,7,3,6,4),
	(213,7,3,7,4),
	(214,7,4,2,4),
	(215,7,4,3,4),
	(216,7,4,4,4),
	(217,7,4,5,4),
	(218,7,4,6,4),
	(219,8,1,1,4),
	(220,8,1,2,4),
	(221,8,1,3,4),
	(222,8,1,4,4),
	(223,8,1,5,4),
	(224,8,1,6,4),
	(225,8,1,7,4),
	(226,8,1,8,4),
	(227,8,1,9,4),
	(228,8,1,10,4),
	(229,8,2,1,4),
	(230,8,2,2,4),
	(231,8,2,3,4),
	(232,8,2,4,4),
	(233,8,2,5,4),
	(234,8,2,6,4),
	(235,8,2,7,4),
	(236,8,2,8,4),
	(237,8,2,9,4),
	(238,8,3,1,4),
	(239,8,3,2,4),
	(240,8,3,3,4),
	(241,8,3,4,4),
	(242,8,3,5,4),
	(243,8,3,6,4),
	(244,8,3,7,4),
	(245,8,4,2,4),
	(246,8,4,3,4),
	(247,8,4,4,4),
	(248,8,4,5,4),
	(249,8,4,6,4),
	(250,9,1,1,4),
	(251,9,1,2,4),
	(252,9,1,3,4),
	(253,9,1,4,4),
	(254,9,1,5,4),
	(255,9,1,6,4),
	(256,9,1,7,4),
	(257,9,1,8,4),
	(258,9,1,9,4),
	(259,9,1,10,4),
	(260,9,2,1,4),
	(261,9,2,2,4),
	(262,9,2,3,4),
	(263,9,2,4,4),
	(264,9,2,5,4),
	(265,9,2,6,4),
	(266,9,2,7,4),
	(267,9,2,8,4),
	(268,9,2,9,4),
	(269,9,3,1,4),
	(270,9,3,2,4),
	(271,9,3,3,4),
	(272,9,3,4,4),
	(273,9,3,5,4),
	(274,9,3,6,4),
	(275,9,3,7,4),
	(276,9,4,2,4),
	(277,9,4,3,4),
	(278,9,4,4,4),
	(279,9,4,5,4),
	(280,9,4,6,4),
	(281,10,1,1,4),
	(282,10,1,2,4),
	(283,10,1,3,4),
	(284,10,1,4,4),
	(286,10,1,5,4),
	(287,10,1,6,4),
	(288,10,1,7,4),
	(289,10,1,8,4),
	(290,10,1,9,4),
	(291,10,1,10,4),
	(292,10,2,1,4),
	(293,10,2,2,4),
	(294,10,2,3,4),
	(295,10,2,4,4),
	(296,10,2,5,4),
	(297,10,2,6,4),
	(298,10,2,7,4),
	(299,10,2,8,4),
	(300,10,2,9,4),
	(301,10,3,1,4),
	(302,10,3,2,4),
	(303,10,3,3,4),
	(304,10,3,4,4),
	(305,10,3,5,4),
	(306,10,3,6,4),
	(307,10,3,7,4),
	(308,10,4,2,4),
	(309,10,4,3,4),
	(310,10,4,4,4),
	(311,10,4,5,4),
	(312,10,4,6,4);

/*!40000 ALTER TABLE `slot` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table time
# ------------------------------------------------------------

DROP TABLE IF EXISTS `time`;

CREATE TABLE `time` (
  `time_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`time_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `time` WRITE;
/*!40000 ALTER TABLE `time` DISABLE KEYS */;

INSERT INTO `time` (`time_id`, `name`)
VALUES
	(1,'09:30 - 10:30'),
	(2,'10:30 - 11:30'),
	(3,'11:30 - 12:30'),
	(4,'12:30 - 13:30'),
	(5,'13:30 - 14:30'),
	(6,'14:30 - 15:30'),
	(7,'15:30 - 16:30'),
	(8,'16:30 - 17:30'),
	(9,'17:30 - 18:30'),
	(10,'18:30 - 19:30');

/*!40000 ALTER TABLE `time` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`)
VALUES
	(1,'lookers','c7259308462a26fd9422e1f6dc779a463845f789');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
