<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('main_m');

		$this->data['title'] = "The Lookers Ford VIP Event";
	}

	public function index()
	{

		$this->form_validation->set_error_delimiters('<div style="margin-top: 15px; max-width: 360px; position: relative; left: 50%; margin-left: -180px; font-size: 14px; padding: 8px" class="alert alert-danger">', '</div>');
		$this->form_validation->set_rules('name', 'full name', 'required');

		$this->form_validation->set_message('required', 'You must insert your %s into the input field above.');

		if ($this->form_validation->run() == TRUE)
		{
			redirect("welcome/".urlencode(base64_encode($this->input->post('name'))));
		}

		$this->layout->view('home', $this->data);
	}

	public function welcome($name)
	{
		( is_string($name) && $name != "" ? TRUE : redirect('/') );
		$this->data['name'] = $name;

		$this->layout->view('welcome', $this->data);
	}

	public function dealers($name)
	{
		( is_string($name) && $name != "" ? TRUE : redirect('/') );
		$this->data['name'] = $name;

		$this->data['dealers'] = $this->main_m->get_dealers();

		$this->layout->view('dealers', $this->data);
	}

	public function date($name, $dealer)
	{
		( is_string($name) && $name != "" ? TRUE : redirect('/') );
		( $this->main_m->is_dealership($dealer) ? TRUE : redirect('welcome/'.$name) );
		$this->data['name']   = $name;
		$this->data['dealer'] = $dealer;

		$this->data['dates']   = $this->main_m->get_dealer_availability_by_day($dealer);

		$this->data['address'] = $this->main_m->get_address($dealer);

		$this->layout->view('date', $this->data);
	}

	public function time($name, $dealer, $date)
	{
		( is_string($name) && $name != "" ? TRUE : redirect('/') );
		( $this->main_m->is_dealership($dealer) ? TRUE : redirect('welcome/'.$name) );
		( $this->main_m->is_date($date) ? TRUE : redirect('dealers/'.$name) );

		$this->data['name']   = $name;
		$this->data['dealer'] = $dealer;
		$this->data['date']   = $date;

		$this->data['times']   = $this->main_m->get_dealer_availability_by_dayTime($dealer, $date);

		$this->data['all_times'] = $this->main_m->get_times();
		$this->data['address'] = $this->main_m->get_address($dealer);

		$this->layout->view('time', $this->data);
	}

	public function review($name, $dealer, $date, $time)
	{
		( is_string($name) && $name != "" ? TRUE : redirect('/') );
		( $this->main_m->is_dealership($dealer) ? TRUE : redirect('welcome/'.$name) );
		( $this->main_m->is_date($date) ? TRUE : redirect('dealers/'.$name) );
		( $this->main_m->is_time($time) ? TRUE : redirect('date/'.$name.'/'.$dealer) );

		$this->data['name']   = $name;
		$this->data['dealer'] = $dealer;
		$this->data['date']   = $date;
		$this->data['time']   = $time;


		$this->data['text_date'] = $this->main_m->get_text_date($date);
		$this->data['text_time'] = $this->main_m->get_text_time($time);

		$this->form_validation->set_error_delimiters('<div style="margin-top: -15px; max-width: 360px; position: relative; left: 50%; margin-left: -180px; font-size: 14px; padding: 8px" class="alert alert-danger">', '</div>');
		$this->form_validation->set_rules('email', 'email address', 'required');
		$this->form_validation->set_rules('phone', 'contact number', 'required');
		$this->form_validation->set_rules('cartype', 'car type', 'required');

		$this->form_validation->set_message('required', 'You must insert your %s into the input field above.');

		if ($this->form_validation->run() == TRUE)
		{
			$url = "/confirm/{$name}/{$dealer}/{$date}/{$time}";
			$url .= "/" . urlencode(base64_encode($this->input->post('email')));
			$url .= "/" . urlencode(base64_encode($this->input->post('phone')));
			$url .= "/" . urlencode(base64_encode($this->input->post('cartype')));
			redirect($url);
		}

		$this->layout->view('review', $this->data);
	}


	public function confirm($name, $dealer, $date, $time, $email, $phone, $type)
	{
		( is_string($name) && $name != "" ? TRUE : redirect('/') );
		( $this->main_m->is_dealership($dealer) ? TRUE : redirect('welcome/'.$name) );
		( $this->main_m->is_date($date) ? TRUE : redirect('dealers/'.$name) );
		( $this->main_m->is_time($time) ? TRUE : redirect('date/'.$name.'/'.$dealer) );
		( is_string($email) && $email != "" ? TRUE : redirect('review/'.$name.'/'.$dealer.'/'.$date.'/'.$time) );
		( is_string($phone) && $phone != "" ? TRUE : redirect('review/'.$name.'/'.$dealer.'/'.$date.'/'.$time) );

		$this->data['name']   = base64_decode(urldecode($name));
		$this->data['dealer'] = $dealer;
		$this->data['date']   = $date;
		$this->data['time']   = $time;
		$this->data['email']  = base64_decode(urldecode($email));
		$this->data['phone']  = base64_decode(urldecode($phone));
		$this->data['type']  = base64_decode(urldecode($type));

		$this->data['text_date'] = $this->main_m->get_text_date($date);
		$this->data['text_time'] = $this->main_m->get_text_time($time);

		$this->data['dealer_data'] = $this->main_m->get_dealer_data($dealer);

		$this->data['address'] = $this->main_m->get_address($dealer);

		// Insert data to database?
		$slot = $this->main_m->get_slot_id($dealer, $date, $time);
		$data = array(
			"slot_id" 			=> $slot,
			"name"    			=> $this->data['name'],
			"email"   			=> $this->data['email'],
			"dealership_id" => $this->data['dealer'],
			"phone"         => $this->data['phone'],
			"cartype"			  => $this->data['type']
		);


		if (!$this->main_m->is_booking($data))
		{
			$this->main_m->save($data);

			$this->load->library('email');

			$this->email->from('no-reply@lookersfordsecret.co.uk', 'Lookers VIP Event');
			// $this->email->to($this->data['email']);
			// $this->email->to('ashley@denfield.co.uk');
			$this->email->cc(array('ashley@denfield.co.uk', 'MichaelHillard@denfield.co.uk'));


			//$emails = $this->main_m->get_mail($this->data['dealer']);

			$cc = array();
			if (isset($emails) && count($emails) && is_array($emails))
			{
				foreach ($emails as $email)
				{
					$cc[] = $email->email;
				}
			}

			if (!empty($cc))
			{
				//$this->email->cc('ashley@denfield.co.uk');
				$this->email->cc($cc);
			}


			$this->email->subject("Booking Confirmation");

			$message = '
			<style>
				font {
					color: #006699 !important;
				}
				a {
					color: #ffffff !important;
					text-decoration: none;
				}
			</style>
				<center>
<table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="600">

  <tr>
   <td><img name="index_r1_c1" src="http://emails.denfield.co.uk/lookers/20161018-vipgenerated/newheader.jpg" width="600" height="514" id="index_r1_c1" alt="" /></td>
  </tr>
  <tr bgcolor="#000000">
   <td class="link" width="600" height="230" valign="top" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff; padding-left:35px" >
   Thank you for booking your appointment at Lookers Ford for <br/>  '.strip_tags($this->data['text_date']).' at '.$this->data['text_time'].'
	 <br /><br />

We look forward to welcoming you to our dealership and discussing your<br />
 requirements in full. <br /><br />

In the meantime, if there is anything we can help you with, please get in<br />
 touch with us and one of our Sales Executives will be happy to help.<br /><br />

'.$this->data['address'].'<br />
   </td>
  </tr>
  <tr>
   <td style="color:#000000" class="link"><a style="color:#ffffff" href="http://www.lookers.co.uk/ford"><img name="index_r3_c1" src="http://emails.denfield.co.uk/lookers/20161018-vipgenerated/footer.jpg" width="600" height="148" id="index_r3_c1" alt="" /></a></td>
  </tr>
  <tr>
   <td width="600" height="78" valign="top" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; padding-left:35px; padding-right:35px">
   Lookers Birmingham Limited (FRN: 403860), MB South Limited (FRN: 461719) and The Dutton-Forshaw Motor Company Limited (FRN: 474287) are appointed representatives of Lookers Motor Group Limited (FRN: 309424) which is authorised and regulated by the Financial Conduct Authority for insurance mediation activities only.</td>
  </tr>
</table>
</center>';

			$this->email->message($message);

			$this->email->send();

			$this->layout->view('confirm', $this->data);
		}
		else
		{
			redirect("/");
		}
	}

}
