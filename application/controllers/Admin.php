<?php
class Admin extends CI_Controller
{
	public function __construct()
	{
			parent::__construct();
			$this->load->model('admin_m');
			$this->load->model('main_m');

			$this->data['title'] = "Lookers Ford | Login";
	}

	public function index()
	{
		// if logged in direct back to dashboard
		($this->session->userdata('authenticated') == TRUE ? redirect('/admin/dashboard') : TRUE);

		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" style="margin-top: 15px;">', '</div>');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$data = array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
			);

			if ($this->admin_m->login($data))
			{
				redirect('/admin/dashboard');
			}
			else
			{
				$this->data['error'] = "Your Username and/or Password combination did not match, please try agian.";
			}


		}

		$this->layout->set_layout("layouts/_login_layout.php");
		$this->layout->view("login", $this->data);
	}

	public function rebuild_db_toyota()
	{
		$limit = 3;

		$dealerships = array(
			'Cambridge' => array(
				'email' => 'sales@cambridge.lancastertoyota.co.uk',
				'address' => '<strong>Lancaster Toyota Cambridge</strong><br>Norman Way, Coldham Lane, <br>Cambridge CB1 3LH<br>Tel: 01223 964338<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
			),
	        'Chelmsford' => array(
	        	'email' => 'sales@chelmsford.lancastertoyota.co.uk',
	        	'address' => '<strong>Lancaster Toyota Chelmsford</strong><br>Hedgerows Business Park, Colchester Road, Springfield,<br>Chelmsford CM2 5PF<br>Tel: 01245 924927<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
	        ),
		    'Colchester' => array(
		    	'email' => 'sales@colchester.lancastertoyota.co.uk',
		    	'address' => '<strong>Lancaster Toyota Colchester</strong><br>Axial Way, Colchester,<br>Essex CO4 5XD<br>Tel: 01206 652847<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
		    ),
	        'Durham' => array(
	        	'email' => 'sales@durham.lancastertoyota.co.uk',
	        	'address' => '<strong>Lancaster Toyota Durham</strong><br>Alma Place, Gilesgate Moor,<br>Durham DH1 2HN<br>Tel: 0191 3706700<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
	        ),
	        'Ipswich' => array(
	        	'email' => 'sales@ipswich.lancastertoyota.co.uk',
	        	'address' => '<strong>Lancaster Toyota Ipswich</strong><br>A1 Augusta Close, Ransomes Europark, Ipswich,<br>Suffolk IP3 9SS<br>Tel: 01473 520682<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
	        ),
	        'Rayleigh' => array(
	        	'email' => 'sales@rayleigh.lancastertoyota.co.uk',
	        	'address' => '<strong>Lancaster Toyota Rayleigh</strong><br>Arterial Road, Rayleigh,<br>Essex SS6 7UQ<br>Tel: 01268 373819<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
	        ),
	        'Romford' => array(
	        	'email' => 'sales@romford.lancastertoyota.co.uk',
	        	'address' => '<strong>Lancaster Toyota Romford</strong><br>Gallows Corner, A127 Southend Arterial Road, Romford,<br>Essex RM3 0DZ<br>Tel: 01708 697814<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
	        ),
	        'St Ives' => array(
	        	'email' => 'sales@stives.lancastertoyota.co.uk',
	        	'address' => '<strong>Lancaster Toyota St Ives</strong><br>Ramsey Road, St Ives,<br>Cambridgeshire PE27 5RE<br>Tel: 01480 741726<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
	        ),
            'Wearside' => array(
            	'email' => 'sales@wearside.lancastertoyota.co.uk',
            	'address' => '<strong>Lancaster Toyota Wearside</strong><br>Newcastle Road, Sunderland,<br>Tyne and Wear SR5 1JE<br>Tel: 0191 5491277<br><a href="http://www.jardinemotors.co.uk/toyota/"><font color="#fff">http://www.jardinemotors.co.uk/toyota/</font></a>'
            )
		);

		$days = array(
			'Friday' => array(
				'name' => 'Friday <br>15<sup>th</sup> January'
			),
			'Saturday' => array(
				'name' => 'Saturday <br>16<sup>th</sup> January'
			),
			'Sunday' => array(
				'name' => 'Sunday <br>17<sup>th</sup> January'
			)
		);

		$times = array(
			'1' => array(
				'name' => '09:00 - 09:45',
				'limit' => 3
			),
			'2' => array(
				'name' => '09:45 - 10:30',
				'limit' => 3
			),
			'3' => array(
				'name' => '10:30 - 11:15',
				'limit' => 3
			),
			'4' => array(
				'name' => '11:15 - 12:00',
				'limit' => 3
			),
			'5' => array(
				'name' => '12:00 - 12:45',
				'limit' => 3
			),
			'6' => array(
				'name' => '14:00 - 14:45',
				'limit' => 3
			),
			'7' => array(
				'name' => '14:45 - 15:30',
				'limit' => 3
			),
			'8' => array(
				'name' => '15:30 - 16:15',
				'limit' => 3
			),
			'9' => array(
				'name' => '16:15 - 17:00',
				'limit' => 3
			),
			'10' => array(
				'name' => '17:00 - 17:45',
				'limit' => 3
			)
		);

		$slots = array();

		foreach ($dealerships as $dealer_name => &$dealer)
		{
			$slot = array();

			$b = array();

			$dealer['name'] = $dealer_name;

			if (!isset($dealer['id']))
			{
				$data = array();
				$data['name'] = $dealer['name'];
				$data['address'] = $dealer['address'];

				$this->db->insert('dealership', $data);
				$dealer['id'] = $this->db->insert_id();

				$dealer_contact = array();
				$dealer_contact['dealership_id'] = $dealer['id'];
				$dealer_contact['email'] = $dealer['email'];

				$this->db->insert('dealership_mail', $dealer_contact);
			}

			$slot['dealership_id'] = $dealer['id'];

			foreach ($days as $day_name => &$day) {

				if (!isset($day['id']))
				{
					$data = array();
					$data['name'] = $day['name'];

					$this->db->insert('day', $data);
					$day['id'] = $this->db->insert_id();
				}

				$slot['day_id'] = $day['id'];

				foreach ($times as $time_key => &$time)
				{
					if (!isset($time['id']))
					{
						$data = array();
						$data['name'] = $time['name'];

						$this->db->insert('time', $data);

						$time['id'] = $this->db->insert_id();
					}

					$slot['time_id'] = $time['id'];
					$slot['num_appointments'] = $time['limit'];

					if ($day_name != 'Sunday' || ($day_name == 'Sunday' && in_array((int)$time_key, array(3,4,5,6,7)))) $this->db->insert('slot', $slot);
				}
			}


		}

		dbug($times);
	}

	public function dashboard($dealerID = NULL, $dayID = NULL)
	{
		$this->data['title'] = "Lookers Ford";

		if ($dealerID != NULL && $dayID == NULL)
		{
			($this->main_m->is_dealership($dealerID) == TRUE ? TRUE : redirect('/admin/dashboard'));

			$this->data['dealerID'] = $dealerID;
			$this->data['days']    = $this->main_m->get_dealer_days($dealerID);
		}
		else if ($dealerID != NULL && $dayID != NULL)
		{
			($this->main_m->is_dealership($dealerID) == TRUE ? TRUE : redirect('/admin/dashboard'));
			($this->main_m->is_date($dayID) == TRUE ? TRUE : redirect('/admin/dashboard'));

			$this->data['dealerID'] = $dealerID;
			$this->data['days']    = $this->main_m->get_dealer_days($dealerID);

			foreach ($this->data['days'] as $day)
			{
				if ($day->day_id == $dayID)
				{
					$this->data['title'] = "Lookers Ford | " . strip_tags($day->name);
				}
			}

			$this->data['bookings'] = $this->main_m->get_bookings_by_dealer_and_time($dealerID, $dayID);
		}

		$this->data['dealers'] = $this->main_m->get_dealers();

		$this->layout->set_layout("layouts/_dashboard_layout.php");
		$this->layout->view("dashboard", $this->data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->sess_regenerate(TRUE);
		redirect('/admin');
	}

}
