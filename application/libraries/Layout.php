<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Layout
{
  private $ci;
  private $layout;

  public function __construct($layout = "layouts/_main_layout")
  {
    $this->ci =& get_instance();
    $this->layout = $layout;
  }

  public function set_layout($layout)
    {
        $this->layout = $layout;
    }

    public function view($view, $data=null, $return=false)
    {
        $loaded_data = array();
        $loaded_data['yield'] = $this->ci->load->view($view, $data, true);
        if ($return)
        {
            $output = $this->ci->load->view($this->layout, $loaded_data, true);
            return $output;
        } 
        else
        {
            $this->ci->load->view($this->layout, $loaded_data, false);
        }
    }
}