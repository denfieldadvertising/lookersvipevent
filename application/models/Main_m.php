<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main_m extends CI_Model {

	public function is_dealership($id)
	{
		$this->db->select('*')
						 ->from("dealership")
						 ->where("dealership_id", $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function is_date($id)
	{
		$this->db->select('*')
						 ->from("day")
						 ->where("day_id", $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function is_time($id)
	{
		$this->db->select('*')
						 ->from("time")
						 ->where("time_id", $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function is_booking($data)
	{
		$this->db->select('*')
						 ->from("bookings")
						 ->where($data);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function get_dealer_data($id)
	{
		$this->db->select('*')
						 ->from("dealership")
						 ->where("dealership_id", $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		return FALSE;
	}

	public function get_text_date($id)
	{
		$this->db->select('*')
						 ->from("day")
						 ->where("day_id", $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->row()->name;
		}
		return FALSE;
	}

	public function get_text_time($id)
	{
		$this->db->select('*')
						 ->from("time")
						 ->where("time_id", $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->row()->name;
		}
		return FALSE;
	}

	public function get_address($id)
	{
		$this->db->select('*')
						 ->from("dealership")
						 ->where("dealership_id", $id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return $query->row()->address;
		}
		return FALSE;
	}


	public function get_dealers()
	{
		$sql = "SELECT * FROM dealership ORDER BY name ASC";
    $query = $this->db->query($sql);

    if($query->num_rows() > 0)
    {
      foreach($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else
    {
      return 0;
    }
	}

	public function get_slots()
	{
		$sql = "SELECT * FROM slot ORDER BY slot_id ASC";
    $query = $this->db->query($sql);

    if($query->num_rows() > 0)
    {
      foreach($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else
    {
      return 0;
    }
	}

	public function get_dates()
	{
		$sql = "SELECT * FROM day";
    $query = $this->db->query($sql);

    if($query->num_rows() > 0)
    {
      foreach($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else
    {
      return 0;
    }
	}

	public function get_times()
	{
		$sql = "SELECT * FROM time";
    $query = $this->db->query($sql);

    if($query->num_rows() > 0)
    {
      foreach($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else
    {
      return 0;
    }
	}

	public function get_bookings_by_dealer($id)
	{
		$sql = "SELECT b.*, s.*, d.name as day, t.name as time FROM bookings b, slot s, day d, time t WHERE b.dealership_id = $id AND b.slot_id = s.slot_id AND s.day_id = d.day_id AND s.time_id = t.time_id";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
    {
      foreach($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else
    {
      return 0;
    }
	}

	public function get_dealer_days($id)
	{
		$sql = "SELECT d.* FROM day d, slot s WHERE s.dealership_id = $id AND d.day_id = s.day_id GROUP BY s.day_id";
		
		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
    {
      foreach($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else
    {
      return 0;
    }
	}

	public function get_bookings_by_dealer_and_time($id, $day)
	{
		$sql = "SELECT b.*, s.*, d.name as day, t.name as time FROM bookings b, slot s, day d, time t WHERE b.dealership_id = $id AND b.slot_id = s.slot_id AND s.day_id = $day AND s.day_id = d.day_id AND s.time_id = t.time_id";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
    {
      foreach($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else
    {
      return 0;
    }
	}

	public function get_available_dealers()
	{
		$dealers   = $this->get_dealers();
		$slots     = $this->get_slots();
		$available = array();

		foreach ($dealers as $dealer)
		{
			foreach ($slots as $slot)
			{
				if ($slot->dealership_id == $dealer->dealership_id)
				{
					$available[] = $dealer;
					break;
				}
			}
		}
	
		return $available;
	}

	public function get_slot_id($dealer, $date, $time)
	{
		$sql = "SELECT * FROM slot WHERE dealership_id = $dealer AND day_id = $date AND time_id = $time LIMIT 1";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0)
		{
			return $query->row()->slot_id;
		}
		return 0;
	}

	public function get_mail($id)
	{
		$sql = "SELECT * FROM dealership_mail WHERE dealership_id = $id";
    $query = $this->db->query($sql);

    if($query->num_rows() > 0)
    {
      foreach($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else
    {
      return 0;
    }
	}

	public function save($data)
	{
		$this->db->insert("bookings", $data);
	}

	function get_dealer_availability_by_day($id) {

		$sql = "SELECT * FROM slot WHERE dealership_id = $id ORDER BY day_id ";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$amountBooking[$row->slot_id] = 0;
			}

			$sql2 = "SELECT * FROM bookings INNER JOIN slot ON bookings.slot_id = slot.slot_id WHERE slot.dealership_id = $id";
			$query2 = $this->db->query($sql2);

			if ($query2->num_rows() > 0)
			{
				foreach ($query2->result() as $row2)
				{
					$amountBooking[$row2->slot_id]++;
				}

				$availableSlot = array();
				foreach ($query->result() as $row)
				{
					if ($amountBooking[$row->slot_id] < $row->num_appointments) {
						$availableSlot[] = $row;
					}
				}

				$days = $this->get_dates();
				$availableDay = array();

				foreach ($days as $day)
				{
					foreach ($availableSlot as $slot)
					{
						if ($slot->day_id == $day->day_id)
						{
							$availableDay[] = $day;
							break;
						}
					}
				}
			}
			else
			{
				$days = $this->get_dates();
				$availableDay = array();
				$keys = array();

				foreach ($query->result() as $row)
				{
					foreach ($days as $day)
					{
						if ($day->day_id == $row->day_id && in_array($day->day_id, $keys) === FALSE)
						{
							$availableDay[] = $day;
							array_push($keys, $day->day_id);
							break;
						}
					}
				}
			}
			return $availableDay;
		}
		return 0;
	}

	public function get_dealer_availability_by_dayTime($dealer, $date)
	{
		$sql = "SELECT * FROM slot WHERE dealership_id = $dealer ORDER BY slot_id";
		$query = $this->db->query($sql);


		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$amountBooking[$row->slot_id] = 0;
			}

			$sql2 = "SELECT b.* FROM bookings b, slot s WHERE b.slot_id = s.slot_id  AND s.dealership_id = $dealer AND s.day_id = $date";
			$query2 = $this->db->query($sql2);

			if ($query2->num_rows() > 0)
			{
				foreach ($query2->result() as $row2)
				{
					$amountBooking[$row2->slot_id]++;
				}


				$availableSlot = array();
				foreach ($query->result() as $row)
				{
					if ($amountBooking[$row->slot_id] < $row->num_appointments) {
						$availableSlot[] = $row;
					}
				}

				$times = $this->get_times();
				$availableTime = array();

				foreach ($times as $time)
				{
					foreach ($availableSlot as $slot)
					{
						if ($slot->day_id == $date && $slot->time_id == $time->time_id)
						{
							$availableTime[] = $time;
							break;
						}
					}
				}
			}
			else
			{
				$times = $this->get_times();
				$availableTime = array();

				foreach ($query->result() as $row)
				{
					foreach ($times as $time)
					{
						if ($row->time_id == $time->time_id && $row->day_id == $date)
						{
							$availableTime[] = $time;
							break;
						}
					}
				}
			}

			foreach ($availableTime as $key => $row)
			{
				$output[$key] = $row->time_id;
			}
			array_multisort($output, SORT_ASC, $availableTime);
			return $availableTime;
		}
		return 0;
	}

}