<?php
class admin_m extends CI_Model
{

	public $_table_name     = 'users';
	public $_table_key      = 'id';
	public $_table_order_by = 'id';

	public function __construct()
	{
			parent::__construct();
	}

	public function login($userdata)
	{
		$data = array(
			"username" => $userdata["username"],
			"password" => sha1($userdata["password"])
		);
		$this->db->select('*')
						 ->from($this->_table_name)
						 ->where($data);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			$this->session->set_userdata('authenticated', TRUE);
			$this->session->set_userdata('id', $query->row()->id);
      $this->session->set_userdata('username', $query->row()->username);
      return TRUE;
		}
		return FALSE;
	}

}