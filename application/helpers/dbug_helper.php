<?php
if (!function_exists('dbug'))
{

	/**
	 * Outputs colored and structured tabular variable information.
	 * 
	 * Variable types supported are: Arrays, Classes/Objects, Database and XML 
	 * Resources.
	 * 
	 * @param type $var
	 */
	function dbug($var)
	{
		$aCallstack=debug_backtrace();
		$text_stack = '';
		$stack =  "<p>CALL STACK</p><p>&nbsp;</p><table border='1'><thead><tr><th>file</th><th>line</th><th>function</th></tr></thead>";
		foreach($aCallstack as $aCall)
		{
			# Format the HTML stack
			if (!isset($aCall['file'])) $aCall['file'] = '[PHP Kernel]';
			if (!isset($aCall['line'])) $aCall['line'] = '';
			$stack .= "<tr><td>{$aCall["file"]}</td><td>{$aCall["line"]}</td<td>{$aCall["function"]}</td></tr>";
			
			# Format the plain text error stack
			if ($text_stack != '') $text_stack .= " --> ";
			$text_stack .= $aCall["file"] . " (" . $aCall["line"] . ") ["  .  $aCall["function"] . "]";
		}
		$stack .= "</table><br /><br />";	
						
		echo $stack;

		$CI = & get_instance();

		$CI->load->library('dbug');

		$CI->dbug->dbug_init($var);

		die();
	}

}