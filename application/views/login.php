<section id="login">
  <div class="login-form admin-form">
      <div class="panel panel-danger mt10 br-n">
        <div class="panel-heading heading-border bg-white">
          <span class="panel-title hidden">
            Sign In
          </span>
        </div>

        <form method="post" action="<?=site_url('admin')?>" id="contact">
          <div class="panel-body bg-light p30">
            <div class="row">
              <div class="col-sm-12 pr30">
                <div class="section">
                  <label for="email" class="field-label text-muted fs18 mb10">Username</label>
                  <input type="text" name="username" id="username" class="gui-input" placeholder="Enter username" value="">
                  <?=form_error('username')?>
                </div>
                <!-- end section -->

                <div class="section">
                  <label for="password" class="field-label text-muted fs18 mb10">Password</label>
                  <input type="password" name="password" id="password" class="gui-input" placeholder="Enter password">
                  <?=form_error('password')?>
                  <?php
                    if (isset($error) && $error)
                    {
                      echo '<div class="alert alert-danger" style="margin-top: 15px;">'.$error.'</div>';
                    }
                  ?>
                </div>
                <!-- end section -->
              </div>
            </div>
          </div>
          <!-- end .form-body section -->
          <div class="panel-footer clearfix p10 ph15">
            <button type="submit" name="login" class="button btn-primary mr10 pull-right">Sign In</button>
          </div>
          <!-- end .form-footer section -->
        </form>
      </div>
    </div>
</section>