<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="keywords" content="<?=$title?>" />
  <meta name="description" content="<?=$title?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Bootstrap -->
  <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/bootstrap/bootstrap.css')?>">

  <!-- Application -->
  <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/style.css')?>">

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?=site_url('assets/img/favicon.ico')?>">

  <script src="<?=site_url('assets/js/jquery.min.js')?>"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <script src="<?=site_url('assets/js/bootstrap/bootstrap.js')?>"></script>
</head>

<body>

  <div id="main">
    <div class="container">
      <div class="col-sm-12">
        <!-- <div class="logo"></div> -->
        <!-- <img class="logo-image" src="/assets/img/logo.png"> -->
        <section id="content-wrapper">
          <div class="logo-small"><img src="/assets/img/logo.png"></div>
          <div class="main-title">
            <h1>GET READY FOR <br>
            THE VIP TREATMENT</h1>
            <p>Join us for our exclusive VIP event</p>
          </div>
          <div class="content">
            <?=$yield?>
          </div>
        </section>
      </div>
    </div>
  </div>

</body>

</html>
