<h2>Complete Bookings Index</h2>
<div class="spacer"></div>
<div id="index">
	<ul class="dealers clearfix">
		<?php
			if (isset($dealers) && count($dealers) && is_array($dealers))
			{
				foreach ($dealers as $dealer)
				{
					echo '<li><a href="'.site_url('admin/dashboard/'.$dealer->dealership_id).'" class="btn btn-danger">'.$dealer->name.'</a></li>';
				}
			}
			else
			{
				$noDealers = TRUE;
			}
		?>
	</ul>
	<?php
		if (isset($noDealers) && $noDealers)
		{
			echo '<p style="padding-top: 15px;">There are currently no dealerships available.</p>';
		}
		else if ( (!isset($bookings)) && (isset($days) && count($days) && is_array($days)) )
		{
			?>
			<p style="padding-top">Please select a date below:</p>
			<ul class="dealers clearfix">
				<?php foreach ($days as $day): ?>
					<li><a href="<?=site_url('admin/dashboard/'.$dealerID.'/'.$day->day_id)?>" class="btn btn-xs btn-info"><?=str_replace("<br>", "", $day->name)?></a></li>
				<?php endforeach; ?>
			</ul>
			<?php
		}
		else if ( (isset($bookings) && count($bookings) && is_array($bookings)) && (isset($days) && count($days) && is_array($days)) )
		{
			?>
			<ul class="dealers clearfix">
				<?php foreach ($days as $day): ?>
					<li><a href="<?=site_url('admin/dashboard/'.$dealerID.'/'.$day->day_id)?>" class="btn btn-xs btn-info"><?=str_replace("<br>", "", $day->name)?></a></li>
				<?php endforeach; ?>
			</ul>
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Name</th>
						<th>Date</th>
						<th>Time</th>
						<th>Email Address</th>
						<th>Contact Number</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($bookings as $booking)
						{
							echo '<tr>';
								echo '<td>'.$booking->name.'</td>';
								echo '<td>'.$booking->day.'</td>';
								echo '<td>'.$booking->time.'</td>';
								echo '<td>'.$booking->email.'</td>';
								echo '<td>'.$booking->phone.'</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
			<a href="javascript:void()" id="print" onclick="window.print();" class="btn btn-danger">Print</a>
			<?php
		}
		else if ( (isset($bookings) && !$bookings) && (isset($days) && count($days) && is_array($days)) )
		{
			?>
				<ul class="dealers clearfix">
					<?php foreach ($days as $day): ?>
						<li><a href="<?=site_url('admin/dashboard/'.$dealerID.'/'.$day->day_id)?>" class="btn btn-xs btn-info"><?=str_replace("<br>", "", $day->name)?></a></li>
					<?php endforeach; ?>
				</ul>
				<p>There are currently no bookings available for this dealership and date.</p>
			<?php
		}
		else
		{
			echo '<p style="padding-top: 15px;">Please select a dealership in order to view related bookings.</p>';
		}
	?>
</div>