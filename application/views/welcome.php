<div class="person-name">Welcome:&nbsp;&nbsp;&nbsp;&nbsp;<span><?=base64_decode(urldecode($name))?></span></div>
<div class="display">
  <p style="font-size:16px;">Welcome to your personal online booking system, here you can book your appointment at our VIP event. We’ve only got a limited number of spaces available, we wouldn’t want you to miss out!</p>
<!--   <p>To help you buy, we've got <span style="font-weight: bold; font-size: 17px;">0% APR</span> Representative Finance^<br/>available on most of our range, plus <strong>YOU DON'T NEED A DEPOSIT*!</strong></p> -->
</div>
<a href="<?=site_url('dealers/'.$name)?>" class="continue">Continue</a><br/><br/><br/><br/><br/><br/><br/><br/>
<p style="font-family: 'FordAntennaRegular'; font-size: 9px; color: #fff; ">Bookings and test drives are subject to availability and change without further notice. Available to over 18s only. Please ensure you bring your valid driving licence to the centre when attending your appointment/test drive. Terms and conditions apply. Lookers Birmingham Limited (FRN: 403860), MB South Limited (FRN: 461719) and The Dutton-Forshaw Motor Company Limited (FRN: 474287) are appointed representatives of Lookers Motor Group Limited (FRN: 309424) which is authorised and regulated by the Financial Conduct Authority for insurance mediation activities only.</p>
