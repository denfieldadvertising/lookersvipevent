<div class="message">CHOOSE YOUR PREFERRED DATE</div>
<div class="display">
  <ul class="date">
    <?php if (isset($dates) && count($dates) && is_array($dates)): ?>
    <?php foreach ($dates as $date): ?>
      <li>
        <?=$date->name?><br/>
        <a href="<?=site_url('time/'.$name.'/'.$dealer.'/'.$date->day_id)?>" class="choose">Choose</a>
      </li>
    <?php endforeach; ?>
    <?php else: ?>
      <li class="display: block; width: 100%; color: white;">We are very sorry, all the dealerships are currently fully booked. Please contact us for futher information. <br/><?=$address?></li>
    <?php endif; ?>
  </ul>
</div>