<div class="message">CHOOSE YOUR PREFERRED TIME</div>
<div class="display">
  <ul class="nav date clearfix">
    <?php
      if ( (isset($times) && count($times) && is_array($times)) && (isset($all_times) && count($all_times) && is_array($all_times)) )
      {
        $array = array();
        foreach ($all_times as $t)
        {
          $match = FALSE;
          foreach ($times as $time)
          {
            if ($time->time_id == $t->time_id)
            {
              $array[$time->time_id] = $time;
              $array[$time->time_id]->status = 1;
              $match = TRUE;
            }
          }
          if (!$match)
          {
            $array[$t->time_id] = $t;
            $array[$t->time_id]->status = 0;
          }
        }


        foreach ($array as $key => $row)
        {
          $output[$key] = $row->time_id;
        }
        array_multisort($output, SORT_ASC, $array);

        foreach ($array as $key => $value)
        {
          if ($value->status)
          {
            echo '<li>
                    '.$value->name.'
                    <a href="'.site_url('review/'.$name.'/'.$dealer.'/'.$date.'/'.$value->time_id).'" class="choose">Choose</a>
                  </li>';
          }
          else
          {
            echo '<li class="disabled" style="opacity: 0.5;">
                    '.$value->name.'
                    <a href="#" class="choose">Choose</a>
                  </li>';
          }

        }

      }
      else
      {
        echo '<li class="display: block; width: 100%; color: white;">We are very sorry, all the times are currently fully booked. Please contact us for futher information. <br/>'.$address.'</li>';
      }

    ?>

  </ul>
</div>
