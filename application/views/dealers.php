<div class="message">CHOOSE YOUR PREFERRED SITE</div>
<div class="display">
  <ul class="date clearfix">
    <?php if (isset($dealers) && count($dealers) && is_array($dealers)): ?>
    <?php foreach ($dealers as $dealer): ?>
      <li>
        <?=$dealer->name?><br/>
        <a href="<?=site_url('date/'.$name.'/'.$dealer->dealership_id)?>" class="choose">Choose</a>
      </li>
    <?php endforeach; ?>
    <?php else: ?>
      <li class="display: block; width: 100%; color: white;">There are currently no dealerships available.</li>
    <?php endif; ?>
  </ul>
</div>
