<div class="all-info">
  <span>Confirmation of your booking details</span></br>
  <?=base64_decode(urldecode($name))?> </br> <?=strip_tags($text_date)?> </br> <?=$text_time?>
</div>
<form name="form" class="confirmation" method="post" action="<?=site_url('review/'.$name.'/'.$dealer.'/'.$date.'/'.$time)?>">
  <!-- <label>Please enter your preferred email address in the box below to receive your confirmation email.</label> -->
  <input type="text" name="email" placeholder="Email Address" /><br/>
  <?=form_error('email')?>
  <!-- <label>Please enter your preferred contact number in the box below so we can contact you if anything changes.</label> -->
  <input type="text" name="phone" placeholder="Contact Number" /><br/>
  <?=form_error('phone')?>

  <select name="cartype">
    <option value="x">Please Select..</option>
    <option value="new">New</option>
    <option value="used">Used</option>
  </select><br/><br/>
  <input type="image" src="<?=site_url('assets/img/confirm.png')?>" alt="Submit Form" />
</form>
